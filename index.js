const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name');

function fullName() {
 	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}


txtFirstName.addEventListener('keyup', fullName);
txtLastName.addEventListener('keyup', fullName);


